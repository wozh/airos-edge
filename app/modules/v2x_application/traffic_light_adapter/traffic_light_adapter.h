/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <memory>
#include <string>

#include "air_service/framework/proto/airos_traffic_light.pb.h"
#include "app/framework/proto/v2xpb-asn-message-frame.pb.h"
#include "base/device_connect/proto/cloud_data.pb.h"

#include "app/framework/interface/app_base.h"
#include "app/modules/v2x_application/common/producer_consumer_queue.h"

namespace airos {
namespace app {

class TrafficLightAdapter {
 public:
  TrafficLightAdapter()
      : spat_count_(1) {}

  ~TrafficLightAdapter(){};
  bool Init(
      ProducerConsumerQueue<std::shared_ptr<os::v2x::device::CloudData>>*
          cloud_sequence,
      const airos::app::ApplicationCallBack& send_cb);
  bool Proc(
      const std::shared_ptr<const airos::trafficlight::TrafficLightServiceData>&
          service_data);

 private:
  bool TrafficLightServicePb2Cloud(
      const std::shared_ptr<const airos::trafficlight::TrafficLightServiceData>&
          service_data,
      std::shared_ptr<os::v2x::device::CloudData> cloud_pb);
  bool TrafficLightServicePb2AsnPb(
      const std::shared_ptr<const airos::trafficlight::TrafficLightServiceData>&
          service_data,
      std::shared_ptr<v2xpb::asn::MessageFrame> asn_pb);
  int64_t get_minute_year();
  int64_t get_mill_second_minute();
  bool send_step_control(int step);

  int spat_count_;
  // max PhaseID 255
  static const uint32_t MAX_VEHICLE_PHASE = 200;
  std::string rscu_sn_;
  std::string MQTT_TRAFFICLIGHT_TOPIC_PREFIX = "upload/trafficlight/";
  ProducerConsumerQueue<std::shared_ptr<os::v2x::device::CloudData>>*
      cloud_sequence_;
  airos::app::ApplicationCallBack send_;
};

}  // namespace app
}  // namespace airos
