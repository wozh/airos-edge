#include "v2x_application.h"

#include <gflags/gflags.h>

#include "app/framework/interface/app_middleware.h"
#include "app/modules/v2x_application/common/app_flag.h"
#include "app/modules/v2x_application/usecase/map/map_info.h"
#include "base/common/log.h"

namespace airos {
namespace app {

bool V2xApplication::Init(const AppliactionConfig& conf) {
  std::string flag_file_path = conf.app_config_path() + "/v2x_config.flag";
  google::SetCommandLineOption("flagfile", flag_file_path.c_str());
  cloud_adapter_ = std::make_shared<CloudAdapter>();
  if (!cloud_adapter_->Init(&cloud_sequence_)) {
    LOG_ERROR << "CLOUD ADAPTER init failed.";
    return false;
  }
  usecase_adapter_ =
      std::make_shared<airos::perception::usecase::UsecaseAdapter>();
  if (!usecase_adapter_->Init(conf.app_config_path())) {
    LOG_ERROR << "USECASE ADAPTER init failed.";
    return false;
  }
  RegisterOSMessage<airos::perception::PerceptionObstacles>(
      OSMessageType::PERCEPTION_OBSTACLES_SERVICE,
      std::bind(
          &airos::perception::usecase::UsecaseAdapter::Proc, usecase_adapter_,
          std::placeholders::_1));
  perception_adapter_ = std::make_shared<PerceptionAdapter>();
  if (!perception_adapter_->Init(&cloud_sequence_)) {
    LOG_ERROR << "PERCEPTION ADAPTER init failed.";
    return false;
  }
  StartThreadPerceptionAdapter();
  rsi_generator_ = std::make_shared<RsiGenerator>();
  if (!rsi_generator_->Init(sender_, conf.app_config_path())) {
    LOG_ERROR << "RSI GENERATOR init failed.";
    return false;
  }
  StartThreadRsiGenerator();
  traffic_light_adapter_ = std::make_shared<TrafficLightAdapter>();
  if (!traffic_light_adapter_->Init(&cloud_sequence_, sender_)) {
    LOG_ERROR << "TRAFFIC LIGHT ADAPTER init failed.";
    return false;
  }
  RegisterOSMessage<::airos::trafficlight::TrafficLightServiceData>(
      OSMessageType::TRAFFIC_LIGHT_SERVICE,
      std::bind(
          &V2xApplication::OnTrafficLightData, this, std::placeholders::_1));
  v2x_message_reporter_ = std::make_shared<V2xMessageReporter>();
  if (!v2x_message_reporter_->Init(&cloud_sequence_)) {
    LOG_ERROR << "V2X MESSAGE REPORTER init failed.";
    return false;
  }
  RegisterOSMessage<::os::v2x::device::RSUData>(
      OSMessageType::RSU_UPSTREAM,
      std::bind(
          &V2xMessageReporter::Proc, v2x_message_reporter_,
          std::placeholders::_1));
  RegisterOSMessage<::os::v2x::device::RSUData>(
      OSMessageType::RSU_DOWNSTREAM,
      std::bind(
          &V2xMessageReporter::ProcRsuInData, v2x_message_reporter_,
          std::placeholders::_1));
  monitor_ = std::make_shared<airos::monitor::Monitor>();
  if (!monitor_->Init(&cloud_sequence_)) {
    LOG_INFO << "MONITOR init failed.";
    return false;
  }
  return true;
}

void V2xApplication::Start() {
  LOG_INFO << "START.";
}

void V2xApplication::OnTrafficLightData(
    const std::shared_ptr<const ::airos::trafficlight::TrafficLightServiceData>&
        data) {
  traffic_light_adapter_->Proc(data);
  airos::perception::usecase::SetTrafficLightState(data);
}

void V2xApplication::StartThreadPerceptionAdapter() {
  b_perception_adapter_ = true;
  thread_perception_adapter_.reset(new std::thread([&] {
    while (b_perception_adapter_) {
      auto perception_data =
          std::make_shared<airos::usecase::EventOutputResult>();
      {
        std::unique_lock<std::mutex> guard(
            airos::perception::usecase::g_usecase_mtx);
        airos::perception::usecase::g_usecase_condition.wait(guard, [] {
          return airos::perception::usecase::g_usecase_for_perception
                     .ByteSizeLong() != 0;
        });
        perception_data->operator=(
            airos::perception::usecase::g_usecase_for_perception);
        airos::perception::usecase::g_usecase_for_perception.Clear();
      }
      perception_adapter_->Proc(perception_data);
    }
  }));
}

void V2xApplication::StartThreadRsiGenerator() {
  b_rsi_generator_ = true;
  thread_rsi_generator_.reset(new std::thread([&] {
    while (b_rsi_generator_) {
      auto rsi_data = std::make_shared<airos::usecase::EventOutputResult>();
      {
        std::unique_lock<std::mutex> guard(
            airos::perception::usecase::g_usecase_mtx);
        airos::perception::usecase::g_usecase_condition.wait(guard, [] {
          return airos::perception::usecase::g_uescase_for_rsi.ByteSizeLong() !=
                 0;
        });
        rsi_data->operator=(airos::perception::usecase::g_uescase_for_rsi);
        airos::perception::usecase::g_uescase_for_rsi.Clear();
      }
      rsi_generator_->Proc(rsi_data);
    }
  }));
}

V2xApplication::~V2xApplication() {
  if (thread_perception_adapter_ && thread_perception_adapter_->joinable()) {
    b_perception_adapter_ = false;
    thread_perception_adapter_->join();
    thread_perception_adapter_.reset();
    LOG_INFO << __FUNCTION__ << "wait thread perception adapter.";
  }

  if (thread_rsi_generator_ && thread_rsi_generator_->joinable()) {
    b_rsi_generator_ = false;
    thread_rsi_generator_->join();
    thread_rsi_generator_.reset();
    LOG_INFO << __FUNCTION__ << "wait thread rsi generator.";
  }
}

AIROS_APPLICATION_REG_FACTORY(V2xApplication, "appv2x")

}  // namespace app
}  // namespace airos
