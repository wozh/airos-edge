#include "app_flag.h"

namespace airos {
namespace app {

DEFINE_int32(rsu_intersection_id, 10, "rsu intersection id");
DEFINE_string(
    city_string, "gzho#", "city name with length is 5 and end with #");
DEFINE_string(asn_message_version, "4layer", "v2x message Asn.1 file version");
DEFINE_string(rsu_xml_map_file, "rsu_map.xml", "xml map file name");
DEFINE_string(cloud_mqtt_addr, "ssl://172.16.227.84:8974", "");
DEFINE_string(cloud_mqtt_user, "v2x", "mqtt user");
DEFINE_string(cloud_mqtt_passwd, "emqx_pass", "mqtt password");
DEFINE_int32(cloud_mqtt_connect_timeout, 10, "mqtt reconnect timeout");
}  // namespace app
}  // namespace airos
