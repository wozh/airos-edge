# Perception Lidar Service

Lidar感知模块的主要任务是接收激光雷达的点云数据，通过算法识别点云中的障碍物。

智路OS未提供Lidar感知的算法示例实现，请根据实际需求，基于Lidar检测框架自行实现。