# AirOS Docker Builder

目前的智路OS提供的镜像包括以下几种：
| 镜像功能             | 功能   | dockerfile   |
| ------------------- | ---------------- | ---------------- |
| X86_64 项目镜像      | 包含基于X86_64的智路OS框架及组件的运行环境和代码，</br>支持工程编译、运行，支持进行智路OS发行版开发。</br>示例感知组件相关驱动依赖见dockerfile| dev.dockerfile |
| AArch64 项目镜像     | 包含基于AArch64的智路OS框架及组件的运行环境和代码，</br>支持工程编译、运行，支持进行智路OS发行版开发。</br>示例感知组件相关驱动依赖见dockerfile| dev.aarch64.orin.dockerfile |
| X86_64 组件开发镜像      | 包含基于X86_64的智路OS框架及示例组件、智路OS SDK、</br>组件开发工具airospkg，为开发者提供工程模板创建、开发、</br>编译、调试、发布等端到端的开发环境 | sdk_amd64.dockerfile |
| AArch64 组件开发镜像     | 包含基于AArch64的智路OS框架及示例组件、智路OS SDK、</br>组件开发工具airospkg，为开发者提供工程模板创建、开发、</br>编译、调试、发布等端到端的开发环境 | sdk_arm64.dockerfile |
| X86_64 Minimal Env  | 最小系统SDK环境，仅包含X86_64智路OS框架、智路OS SDK、</br>组件开发工具airospkg，不包含NVIDIA驱动、CUDA等 </br>支持进行非NVIDIA或CPU推理的组件开发| sdk_base.dockerfile |
| AArch64 Minimal Env | 最小系统SDK环境，仅包含AArch64智路OS框架、智路OS SDK、</br>组件开发工具airospkg，不包含NVIDIA驱动、CUDA等 </br>支持进行非NVIDIA或CPU推理的组件开发 | sdk_base.dockerfile |

## 1 镜像构建
**项目镜像**
```bash
bash build.sh
```
**SDK镜像**

[SDK镜像构建](../../package/README.md)

## 2 镜像运行
**项目镜像**
```bash
# 请在源代码根目录执行
bash docker/scripts/docker_start.sh
bash docker/scripts/docker_into.sh
```

**SDK镜像**
```bash
# 请在源代码根目录执行
bash docker/scripts/docker_start.sh sdk
bash docker/scripts/docker_into.sh sdk
```

## 3 智路OS Minimal Environment
智路OS提供了一种高度开放的智路OS开发环境，开发者可以根据自己的AI硬件定制组件开发镜像。最小系统仅包含智路OS框架、智路OS SDK、组件开发工具airospkg。

### 3.1 镜像拉取与运行
智路OS提供了基于Ubuntu 20.04的最小系统镜像，无AI及推理框架相关依赖(不包含NVIDIA驱动、PaddlePaddle、TensorRT依赖库等)。可以直接拉取后进行组件开发。
```bash
# X86_64镜像
docker pull registry.baidubce.com/zhiluos/airos:base-amd64-20240301-sdk
# AArch64镜像
docker pull registry.baidubce.com/zhiluos/airos:base-arm64-20240301-sdk
```
镜像运行及组件开发参考3.6与3.7


### 3.2 dockerfile修改
`dev.base.dockerfile`是智路OS框架的基本运行环境。

如有更换底层操作系统的需求，可以修改`dev.base.dockerfile`中的`FROM`字段与软件源。并基于实际运行情况及需求重新编译依赖库。依赖库源如下:
```bash
asn-wrapper.tar.xz   https://gitee.com/ZhiluCommunity/airos-v2x-msg
cyber-rt.tar.xz  https://github.com/ApolloAuto/apollo/tree/v8.0.0
fast-rtps-1.5.0-1.tar.xz https://github.com/eProsima/Fast-RTPS.git
```

### 3.3 基础镜像构建与运行
```bash
cd docker/builder
# 构建X86_64镜像
docker build -t airos_framework:v1 -f dev.base.dockerfile dev-amd64
# 构建AArch64镜像
docker build -t airos_framework:v1 -f dev.base.dockerfile dev-arm64

# 镜像运行
# 工程根目录
cd airos-edge
docker run -itd \
    --name airos_framework_img \
    -v "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P):/airos" \
    -w "/airos" \
    airos_framework:v1 \
    /bin/bash
```
### 3.4 智路OS集成环境产出
```bash
docker exec -it airos_framework_img \
    bash -c 'bash build_airos.sh framework && \
    bash package/make_sdk.sh && \
    cd package && \
    bash make_pkg.sh'
```

### 3.5 智路OS SDK镜像构建
```bash
# 创建airos sdk镜像
bash package/make_sdk_img.sh base
```
### 3.6 镜像运行
```bash
docker run -itd \
    --name "airos-sdk-base" \
    --net host \
    -v "/media:/media" \
    -v "/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    -v "/etc/localtime:/etc/localtime:ro" \
    -e USER="${USER}" \
    -e DOCKER_USER="${USER}" \
    -e DOCKER_USER_ID="$(id -u)" \
    -e DOCKER_GRP="$(id -g -n)" \
    -e DOCKER_GRP_ID="$(id -g)" \
    -w "/home/airos/os" \
    registry.baidubce.com/zhiluos/airos:base-amd64-XXX-sdk \
    /bin/bash

# 容器增加非root用户
docker cp docker/scripts/utils/docker_start_user.sh \
    airos-sdk-base:/home/docker_start_user.sh
docker exec -u root -it airos-sdk-base \
    bash -c '/home/docker_start_user.sh'

# 进入容器
docker exec -u "${USER}" -it airos-sdk-base /bin/bash
```
### 3.7 组件开发
在开发组件时，请遵循[智路OS开发手册](https://airos-edge.readthedocs.io/)

airospkg创建的感知相关组件模板cmake和bazel构建脚本默认集成了CUDA依赖。如果不希望使用CUDA，需要手动删除这些依赖。此外，在编译过程中，请确保添加`-DPERCEPTION_CPU_ONLY`选项，可以确保在不使用CUDA的情况下进行编译。

以感知融合开发为例：
```bash
airospkg -type cmake -name fusion_demo
# 修改感知融合代码...
airospkg make -type cmake -name fusion_demo
airospkg release -name fusion_demo -out fusion_demo.tgz
airospkg install -file /home/airos/projs/fusion_demo/fusion_demo.tgz
airospkg run -name fusion_demo -type fusion
```
> Tips：在智路OS的架构中，相机接入组件与单相机感知组件集成在同一个模块，因此两个组件必须同时安装后模块才能正常运行。最小系统中没有提供默认的示例组件，因此，在开发单相机感知组件时，需要先创建一个默认提供的相机接入组件，或者由开发者自定义相机接入组件获取自己的离线或者在线数据进行测试。在开发相机接入模块时，可以创建并安装一个默认提供的的单相机感知模块。