FROM docker.io/library/ubuntu:20.04

SHELL ["/bin/bash", "-c"]

ENV  DEBIAN_FRONTEND noninteractive
COPY tuna-ubuntu-2004-sources.list    /etc/apt/sources.list
RUN yes | unminimize
RUN apt  update                                                            \
 && apt  install -y --no-install-recommends                                \
         zip unzip bzip2 xz-utils gzip zstd vim less file gawk telnet lsof \
         sudo wget curl udev ssh iputils-ping git git-gui gitk git-lfs zsh \
         jq locales locales-all lsb-release webp gfortran rsync man tree   \
         kmod meld default-jre openssl squashfs-tools rsyslog bc           \
                                                                           \
         build-essential autoconf automake bison flex libtool-bin          \
         ltrace strace help2man texinfo device-tree-compiler g++-7         \
         ninja-build cmake nasm                                            \
                                                                           \
         python3                       python                              \
         python3-all-dev               python-all-dev                      \
         python3-crypto                python-crypto                       \
         python3-pexpect               python-pexpect                      \
         python3-setuptools            python-setuptools                   \
         python3-six                   python-six                          \
         python3-yaml                  python-yaml                         \
         python3-requests                                                  \
         python3-pandas                                                    \
         python3-pip                                                       \
                                                                           \
         gstreamer1.0-plugins-base                                         \
         gstreamer1.0-plugins-good                                         \
         libatlas-base-dev                                                 \
         libboost-all-dev                                                  \
         libbz2-dev                                                        \
         libcurl4-openssl-dev                                              \
         libeigen3-dev                                                     \
         libffi-dev                                                        \
         libfreetype6-dev                                                  \
         libgbm1                                                           \
         libgl1-mesa-dev                                                   \
         libglew-dev                                                       \
         libglfw3-dev                                                      \
         libglm-dev                                                        \
         libgstreamer1.0-dev                                               \
         libgtk2.0-dev                                                     \
         libhdf5-dev                                                       \
         libjsoncpp-dev                                                    \
         liblapack-dev                                                     \
         libleveldb-dev                                                    \
         liblz4-dev                                                        \
         liblzma-dev                                                       \
         libncurses5                                                       \
         libncurses-dev                                                    \
         libnuma-dev                                                       \
         libopenblas-dev                                                   \
         libpugixml-dev                                                    \
         libsdl2-dev                                                       \
         libsdl2-image-dev                                                 \
         libsnappy-dev                                                     \
         libssl-dev                                                        \
         libtheora0                                                        \
         libunwind-dev                                                     \
         libv4l-dev                                                        \
         libva-dev                                                         \
         libva-drm2                                                        \
         libvdpau1                                                         \
         libvorbisenc2                                                     \
         libyaml-cpp-dev                                                   \
         libz-dev                                                          \
         uuid-dev                                                          \
 && :

WORKDIR /tmp
# --------------------------------------------------------------------------
ARG PKG=paho.mqtt.c
ARG VER=1.3.13
RUN wget -q --content-disposition                                          \
    https://github.com/eclipse/${PKG}/archive/refs/tags/v${VER}.tar.gz
RUN tar -xf ${PKG}-${VER}.tar.gz                                           \
 && cmake -S ${PKG}-${VER}                                                 \
    -B ${PKG}-${VER}-build                                                 \
    "-DCMAKE_INSTALL_PREFIX=/usr/local/"                                   \
    "-DCMAKE_BUILD_TYPE=Release" "-DBUILD_TESTS=OFF" "-DBUILD_TESTING=OFF" \
    "-DCMAKE_SKIP_INSTALL_RPATH=ON" "-DCMAKE_POSITION_INDEPENDENT_CODE=ON" \
    "-DBUILD_SHARED_LIBS=ON" "-DBUILD_STATIC_LIBS=ON"                      \
    "-DPAHO_BUILD_SHARED=ON"                                               \
    "-DPAHO_BUILD_STATIC=ON"                                               \
    "-DPAHO_WITH_SSL=ON"                                                   \
    "-DPAHO_ENABLE_TESTING=OFF"                                            \
 && make -C ${PKG}-${VER}-build -j$(nproc) install/strip                   \
 && rm -rf ${PKG}-${VER} ${PKG}-${VER}-build
# --------------------------------------------------------------------------
ARG PKG=gflags
ARG VER=2.2.2
RUN wget -q --content-disposition                                          \
    https://github.com/gflags/${PKG}/archive/refs/tags/v${VER}.tar.gz
RUN tar -xf ${PKG}-${VER}.tar.gz                                           \
 && cmake -S ${PKG}-${VER}                                                 \
    -B ${PKG}-${VER}-build                                                 \
    "-DCMAKE_INSTALL_PREFIX=/usr/local/"                                   \
    "-DCMAKE_BUILD_TYPE=Release" "-DBUILD_TESTS=OFF" "-DBUILD_TESTING=OFF" \
    "-DCMAKE_SKIP_INSTALL_RPATH=ON" "-DCMAKE_POSITION_INDEPENDENT_CODE=ON" \
    "-DBUILD_SHARED_LIBS=ON" "-DBUILD_STATIC_LIBS=ON"                      \
 && make -C ${PKG}-${VER}-build -j$(nproc) install/strip                   \
 && rm -rf ${PKG}-${VER}  ${PKG}-${VER}-build
# --------------------------------------------------------------------------
ARG PKG=glog
#ARG VER=0.6.0
ARG VER=0.4.0
RUN wget -q --content-disposition                                          \
    https://github.com/google/${PKG}/archive/refs/tags/v${VER}.tar.gz
RUN tar -xf ${PKG}-${VER}.tar.gz                                           \
 && cmake -S ${PKG}-${VER}                                                 \
    -B ${PKG}-${VER}-build                                                 \
    "-DCMAKE_INSTALL_PREFIX=/usr/local/"                                   \
    "-DCMAKE_BUILD_TYPE=Release" "-DBUILD_TESTS=OFF" "-DBUILD_TESTING=OFF" \
    "-DCMAKE_SKIP_INSTALL_RPATH=ON" "-DCMAKE_POSITION_INDEPENDENT_CODE=ON" \
    "-DBUILD_SHARED_LIBS=ON" "-DBUILD_STATIC_LIBS=ON"                      \
    "-DWITH_GTEST=OFF"                                                     \
    "-DWITH_THREADS=ON"                                                    \
    "-DWITH_UNWIND=ON"                                                     \
    "-DWITH_TLS=OFF"                                                       \
    "-DWITH_GFLAGS=OFF"                                                    \
 && make -C ${PKG}-${VER}-build -j$(nproc) install/strip                   \
 && rm -rf ${PKG}-${VER}  ${PKG}-${VER}-build
# --------------------------------------------------------------------------
ARG PKG=protobuf
ARG VER=3.14.0
RUN wget -q --content-disposition                                          \
    https://github.com/protocolbuffers/${PKG}/archive/refs/tags/v${VER}.tar.gz
RUN tar -xf ${PKG}-${VER}.tar.gz                                           \
 && cmake -S ${PKG}-${VER}/cmake                                           \
    -B ${PKG}-${VER}-build                                                 \
    "-DCMAKE_INSTALL_PREFIX=/usr/local/"                                   \
    "-DCMAKE_BUILD_TYPE=Release" "-DBUILD_TESTS=OFF" "-DBUILD_TESTING=OFF" \
    "-DCMAKE_SKIP_INSTALL_RPATH=ON" "-DCMAKE_POSITION_INDEPENDENT_CODE=ON" \
    "-DBUILD_SHARED_LIBS=ON" "-DBUILD_STATIC_LIBS=ON"                      \
    "-Dprotobuf_BUILD_SHARED_LIBS=ON"                                      \
    "-Dprotobuf_BUILD_TESTS=OFF"                                           \
 && make -C ${PKG}-${VER}-build -j$(nproc) install/strip                   \
 && pushd ${PKG}-${VER}/python >/dev/null                                  \
 && ldconfig                                                               \
 && ./setup.py install                                                     \
 && popd >/dev/null                                                        \
 && rm -rf ${PKG}-${VER}  ${PKG}-${VER}-build
# --------------------------------------------------------------------------
RUN ldconfig

# ==========================================================================
COPY bin /usr/local/bin/

ADD  fast-rtps-1.5.0-1.tar.xz  /usr/local
ADD  asn-wrapper.tar.xz        /opt/local/asn-wrapper
ADD  cyber-rt.tar.xz           /opt/local
RUN  mkdir -p /etc/v2x && echo RSCU_DEFAULT_SN > /etc/v2x/SN
RUN  ldconfig
