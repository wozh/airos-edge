FROM airos_framework:v1

RUN mkdir -p /home/airos
RUN mkdir -p /opt/airos
COPY airos /home/airos
COPY sdk /opt/airos/sdk
COPY package /opt/airos/package

ENV PATH "/opt/airos/package/bin:${PATH}"
